import { createApp } from "vue";
import { createPinia } from "pinia";
import ElementPlus from "element-plus";
import "element-plus/dist/index.css";
import "virtual:windi.css";
import * as ElementPlusIconsVue from "@element-plus/icons-vue";
import store from "./stores/index";
import App from "./App.vue";
import router from "./router";

const app = createApp(App);
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component);
}

app.use(createPinia());
app.use(router);
app.use(ElementPlus);
app.use(store);
import "./router/permission";
import "nprogress/nprogress.css";

app.mount("#app");
