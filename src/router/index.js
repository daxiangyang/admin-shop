import { createRouter, createWebHashHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";
import Admin from "../layouts/admin.vue";

const router = createRouter({
  history: createWebHashHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: Admin,
      // 子路由
      children:[{
        path: "/",
        name: "home",
        component: HomeView,
        meta: {
          title: "后台首页"
        }
      }]
    },
    {
      path: "/login",
      name: "login",
      component: () => import("../views/login.vue"),
      meta: {
        title: "登录页"
      }
    },
    {
      path: "/:pathMatch(.*)*",
      name: "NotFound",
      component: () => import("../views/404.vue"),
    },
  ],
});

export default router;
