import axios from "axios";
import { toast } from "@/composables/util";
import { getToken } from "@/composables/auth";
import store from "@/stores/index";


const service = axios.create({
  baseURL: "/api",
});
// 添加请求拦截器
service.interceptors.request.use(
  function (config) {
    // 在发送请求之前做些什么
    // 往header头部自动添加cookie
    const token = getToken();

    if (token) {
      config.headers["token"] = token;
    }
    return config;
  },
  function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
  }
);

// 添加响应拦截器
service.interceptors.response.use(
  function (response) {
    // 对响应数据做点什么
    return response.data.data;
  },
  function (error) {
    // 对响应错误做点什么
    const msg = error.response.data.msg || "请求失败"

    // 服务端token失效后不能强制修改密码
    if(msg == "非法token，请先登录！"){
      store.dispatch("logout").finally(()=>location.reload())//退出后刷新一下页面
    }

    toast(msg, "error");

    /* ElNotification({
      message: error.response.data.msg || "请求失败",
      type: 'error',
      duration:3000
  }) */
    return Promise.reject(error);
  }
);

export default service;
